-- Adminer 4.8.1 MySQL 8.0.31 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `user_id` int NOT NULL,
  `image_id` int NOT NULL,
  `comment_date` date NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `comment_id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`user_id`,`image_id`,`comment_id`),
  KEY `comment_id` (`comment_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_ibfk_4` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `image_id` int NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`image_id`),
  KEY `image_user_id_foreign` (`user_id`),
  CONSTRAINT `image_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `image` (`image_id`, `image_name`, `image_link`, `desc`, `user_id`) VALUES
(21,	'RedBull',	'link4',	NULL,	18),
(22,	'Sting',	'link4',	NULL,	18),
(23,	'Sting',	'link4',	NULL,	4),
(24,	'Alcohol',	'Link Alcohol',	NULL,	4),
(25,	'Alcohol',	'Link Alcohol',	'Thơm ngon bổ dưỡng',	4),
(26,	'Alcohol',	'Link Alcohol',	'Thơm ngon bổ dưỡng',	18),
(27,	'Beer',	'Link Alcohol',	'Thơm ngon bổ dưỡng',	18);

DROP TABLE IF EXISTS `image_saving`;
CREATE TABLE `image_saving` (
  `user_id` int NOT NULL,
  `image_id` int NOT NULL,
  `saving_date` date NOT NULL,
  PRIMARY KEY (`user_id`,`image_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `image_saving_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `image_saving_ibfk_4` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `image_saving` (`user_id`, `image_id`, `saving_date`) VALUES
(17,	25,	'2023-03-30'),
(18,	22,	'2023-03-30');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `user` (`user_id`, `email`, `password`, `name`, `age`, `avatar`) VALUES
(4,	'ngdat111@gmail.com',	'$2b$10$gfbKw.6W84BQFhnDk/EMF.0l5uERwS/inNj7JwR.fkJFMgoq9DKS2',	'dat22',	182,	''),
(13,	'ngdat523@gmail.com',	'$2b$10$vS/khZYCtz8SGSj4jFLEsuS.DRDWnF1qjG9rZ9DM/oFjFFl88jJ8m',	'dat22',	182,	''),
(14,	'ngdat1@gmail.com',	'$2b$10$g1Ws9/hxRfk0OAIxpfM7Seb9AX22zIXqk9NeJs4OaeShV8tFDy0P.',	'Pop',	19,	'https://cafefcdn.com/thumb_w/650/pr/2023/photo1677721722100-1677721722275122259556-63813463528133.png'),
(15,	'ngdat112@gmail.com',	'$2b$10$Cg.cDU9KT4e3Z1lfTCfSZ.vP9NMfXCrpTKhmN3J7IHFyQNsI9I9ge',	'Pop',	19,	'https://cafefcdn.com/thumb_w/650/pr/2023/photo1677721722100-1677721722275122259556-63813463528133.png'),
(16,	'nake@gmail.com',	'$2b$10$I2vY7OW.lU8EkeluvgPze.qH8w2dbwc1yxJ7dQXsft0MTrzF7NRZG',	'Pop',	19,	'https://cafefcdn.com/thumb_w/650/pr/2023/photo1677721722100-1677721722275122259556-63813463528133.png'),
(17,	'nas@gmail.com',	'$2b$10$bbkM3FYVnXETsDdNszzPsOm2.Mtyegbhjo3zfjwXK/gjO.GGYxHQm',	'Pop',	19,	'https://cafefcdn.com/thumb_w/650/pr/2023/photo1677721722100-1677721722275122259556-63813463528133.png'),
(18,	'new@gmail.com',	'$2b$10$94IdD5df68YLfv7y0fuMRuDy09s1Gj53JFWGT1jylUnmvNLBGa7R.',	'dat22',	182,	''),
(19,	'new1@gmail.com',	'$2b$10$ftDj0tleqvhQWzbAOjmTKu2ef/YQlISCrcmU.bydp3UkN7bE4oB0C',	'Pop',	19,	'https://cafefcdn.com/thumb_w/650/pr/2023/photo1677721722100-1677721722275122259556-63813463528133.png'),
(20,	'new12@gmail.com',	'$2b$10$VC2VDF572UNXOYwKbImGquDRRd.zdbbCJnuaLGVJKMxDOB1sIVKP2',	'Pop',	19,	'https://cafefcdn.com/thumb_w/650/pr/2023/photo1677721722100-1677721722275122259556-63813463528133.png');

-- 2023-03-30 15:57:21
