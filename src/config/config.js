require("dotenv").config();
module.exports = {
  dbDatabase: process.env.DB_DATABASE,
  dbHost: process.env.DB_HOST,
  dbPort: process.env.DB_PORT,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_PASSWORSD,
  dbDialect: process.env.db_dialect,
  privateKey: process.env.private_key,
};
