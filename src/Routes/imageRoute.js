const express = require("express");
const {
  getAllImage,
  getImageByName,
  getImageAndAuthorById,
  getSaveImageByUserId,
  getCreatedImageByUserId,
  checkSaving,
  deleteImageById,
  addImage,
} = require("../Cotrollers/imageController");
// Verify tại rootRoute
const imageRoute = express.Router();
// Lấy tất cả ảnh
imageRoute.get("/getAllImage", getAllImage);
// Lấy ảnh theo tên
imageRoute.get("/getImageByName", getImageByName);
// Lấy thông tin ảnh và người tạo ảnh bằng id ảnh
imageRoute.get("/getImageAndAuthorById", getImageAndAuthorById);
// Kiểm tra xem người dùng đã lưu ảnh này chưa
imageRoute.get("/checkSaving", checkSaving);
// Lấy danh sách ảnh được lưu theo user_id
imageRoute.get("/getSaveImageByUserId/:user_id", getSaveImageByUserId);
// Lấy danh sách ảnh đã tạo theo user_id
imageRoute.get("/getCreatedImageByUserId/:user_id", getCreatedImageByUserId);
// Xoá ảnh theo id ảnh
imageRoute.delete("/deleteImageById", deleteImageById);
// Thêm hình ảnh của User
imageRoute.post("/addImage", addImage);

module.exports = imageRoute;
