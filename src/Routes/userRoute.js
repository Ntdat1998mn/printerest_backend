const express = require("express");
const {
  logIn,
  register,
  getUserInforById,
  updateUser,
  changePassword,
} = require("../Cotrollers/userController");
const { verifyToken } = require("../utils/jwt");
const userRoute = express.Router();

// Đăng nhập
userRoute.post("/LogIn", logIn);
// Đăng kí
userRoute.post("/Register", register);
// Lấy thông tin User theo Id
userRoute.get("/getUserInforById/:user_id", verifyToken, getUserInforById);
// Chỉnh sửa thông tin cá nhân (Không bao gồm password)
userRoute.put("/updateUser", verifyToken, updateUser);
// Chỉnh sửa password (user_id được lấy từ token sau khi đăng nhập)
userRoute.put("/changePassword", verifyToken, changePassword);

module.exports = userRoute;
