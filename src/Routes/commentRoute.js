const express = require("express");
const {
  getCommentById,
  createComment,
} = require("../Cotrollers/commentController");

// Verify tại rootRoute
const commentRoute = express.Router();
// Lấy thông tin bình luận theo id ảnh
commentRoute.get("/getCommentById", getCommentById);
// Lưu thông tin bình luận của người dùng với hình ảnh
commentRoute.post("/createComment", createComment);

module.exports = commentRoute;
