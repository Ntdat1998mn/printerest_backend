const express = require("express");
const { verifyToken } = require("../utils/jwt");
const commentRoute = require("./commentRoute");
const rootRoute = express.Router();

const imageRoute = require("./imageRoute");
const userRoute = require("./userRoute");
// User Route
rootRoute.use("/user", userRoute);
// Image Route
rootRoute.use("/image", verifyToken, imageRoute);
// Comment Route
rootRoute.use("/comment", verifyToken, commentRoute);

module.exports = rootRoute;
