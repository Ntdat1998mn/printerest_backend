/**
 * @swagger
 * /api/capstone-1/user/LogIn:
 *  post:
 *      description: Login with email and password
 *      tags: [User]
 *      parameters:
 *      - in: body
 *        name: user
 *        schema:
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *      responses:
 *          201:
 *              description: User authenticated
 *          401:
 *              description: Invalid email or password
 */
