const { Op } = require("sequelize");
const { successCode, errorCode, failCode } = require("../config/response");
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const { checkEmpty } = require("../services/validate");
const model = initModels(sequelize);

// Lấy tất cả ảnh
const getAllImage = async (req, res) => {
  try {
    let data = await model.image.findAll();
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách ảnh thành công!");
    } else {
      failCode(res, data, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy ảnh theo tên ảnh
const getImageByName = async (req, res) => {
  try {
    let { image_name } = req.body;
    let data = await model.image.findAll({
      where: { image_name: { [Op.like]: `%${image_name}%` } },
    });
    if (data.length > 0) {
      successCode(res, data, "Tìm hình ảnh theo tên thành công!");
    } else failCode(res, data, "Không có dữ liệu!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy thông tin đã lưu hình này chưa theo id ảnh (dùng để kiểm tra đã lưu ở nút save hay chưa)

const checkSaving = async (req, res) => {
  try {
    let { user_id } = req.user.content;
    let { image_id } = req.body;
    let data = await model.image_saving.findOne({
      where: { image_id, user_id },
    });
    if (data) {
      successCode(res, data, `Người dùng có id là ${user_id} đã lưu ảnh này!`);
    } else {
      failCode(res, data, `Người dùng có id là ${user_id} chưa lưu ảnh này!`);
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy thông tin ảnh và người tạo ảnh bằng id ảnh
const getImageAndAuthorById = async (req, res) => {
  try {
    let { image_id } = req.body;
    let data = await model.image.findOne({
      where: { image_id },
      include: "user",
    });
    if (data) {
      successCode(
        res,
        data,
        "Lấy dữ ảnh và người đăng ảnh theo id ảnh thành công!"
      );
    } else {
      failCode(res, data, "Không tìm thấy ảnh có id theo yêu cầu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy danh sách ảnh được lưu theo user_id

const getSaveImageByUserId = async (req, res) => {
  try {
    let { user_id } = req.params;
    let user = await model.user.findByPk(user_id);
    if (checkEmpty(res, user, "Người dùng không tồn tại!")) {
      let data = await model.image_saving.findAll({
        where: { user_id },
      });
      if (data.length > 0) {
        successCode(
          res,
          data,
          "Lấy danh sách ảnh được lưu theo người dùng thành công!"
        );
      } else {
        failCode(res, data, "Người dùng chưa lưu ảnh nào!");
      }
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy danh sách ảnh đã tạo theo user_id

const getCreatedImageByUserId = async (req, res) => {
  try {
    let { user_id } = req.params;
    let user = await model.user.findByPk(user_id);
    if (checkEmpty(res, user, "Người dùng không tồn tại!")) {
      let data = await model.image.findAll({ where: { user_id } });
      if (data.length > 0) {
        successCode(
          res,
          data,
          "Lấy danh sách ảnh được đăng tải theo người dùng thành công!"
        );
      } else {
        failCode(res, data, "Người dùng chưa đăng ảnh nào!");
      }
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Xoá ảnh theo id ảnh

const deleteImageById = async (req, res) => {
  try {
    let { image_id } = req.body;
    let { user_id } = req.user.content;
    let data = await model.image.findOne({ where: { image_id, user_id } });

    if (
      checkEmpty(res, data, "Ảnh không tồn tại trong kho của người dùng này!")
    ) {
      await data.destroy();
      successCode(res, data, "Xoá ảnh đã đăng thành công!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Thêm hình ảnh của User
const addImage = async (req, res) => {
  try {
    let { user_id } = req.user.content;
    let user = await model.user.findByPk(user_id);
    if (checkEmpty(req, user, "Người dùng không tồn tại!")) {
      let { image_name, image_link, desc } = req.body;
      let newImage = { image_name, image_link, desc, user_id };
      let data = await model.image.create(newImage);
      if (data) {
        successCode(res, data, "Thêm ảnh thành công!");
      } else {
        failCode(res, data, "Có lỗi!");
      }
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getAllImage,
  getImageByName,
  checkSaving,
  getImageAndAuthorById,
  getSaveImageByUserId,
  getCreatedImageByUserId,
  deleteImageById,
  addImage,
};
