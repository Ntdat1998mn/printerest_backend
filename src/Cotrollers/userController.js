const { successCode, errorCode, failCode } = require("../config/response");
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const bcrypt = require("bcrypt");
const { creatToken } = require("../utils/jwt");
const { Op, where } = require("sequelize");
const model = initModels(sequelize);

// Đăng nhập
const logIn = async (req, res) => {
  try {
    let { email, password } = req.body;
    let data = await model.user.findOne({ where: { email } });
    if (data) {
      let checkPassword = bcrypt.compareSync(password, data.password);
      if (checkPassword) {
        let token = creatToken(data);
        /* Gửi token vào cookies của web */
        res.cookie("token", token, {
          httpOnly: true,
          /* secure: true, */
          maxAge: 86400000,
        });

        successCode(res, data, "Đăng nhập thành công!");
      } else {
        failCode(res, data, "Mật khẩu không đúng!");
      }
    } else {
      failCode(res, { email, password }, "Email không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

//Đăng ký
const register = async (req, res) => {
  try {
    let { email, password, name, age, avatar } = req.body;
    let newUser = {
      email,
      password: bcrypt.hashSync(password, 10),
      name,
      age,
      avatar,
    };
    let checkEmail = await model.user.findOne({ where: { email } });
    if (checkEmail) {
      failCode(res, newUser, "Email đã tồn tại!");
    } else {
      let data = await model.user.create(newUser);
      successCode(res, data, "Đăng ký thành công!");
    }
  } catch (err) {
    errorCode(res, "Lỗi BackEnd!");
  }
};

// Lấy thông tin người dùng bằng
const getUserInforById = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await model.user.findByPk(user_id);
    if (data) {
      successCode(
        res,
        data,
        `Lấy thông tin người dùng có id là ${user_id} thành công!`
      );
    } else {
      failCode(res, data, `Không có người dùng có ai là ${user_id}`);
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};
// Chỉnh sửa thông tin cá nhân (Không bao gồm password)
const updateUser = async (req, res) => {
  try {
    let { user_id } = req.user.content;
    let { email, name, age, avatar } = req.body;
    let data = await model.user.findAll({
      where: { email, user_id: { [Op.ne]: user_id } },
    });
    let userModel = { email, name, age, avatar };

    if (data.length == 0) {
      await model.user.update(userModel, { where: { user_id } });
      successCode(
        res,
        { user_id, ...userModel },
        "Cập nhật thông tin người dùng thành công!"
      );
    } else {
      failCode(res, { user_id, ...userModel }, "Email đã tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Chỉnh sửa password (user_id được lấy từ token sau khi đăng nhập)

const changePassword = async (req, res) => {
  try {
    let { user_id } = req.user.content;
    let { password } = req.body;
    let data = await model.user.findByPk(user_id);
    let checkPassword = bcrypt.compareSync(password, data.password);
    if (!checkPassword) {
      let userModel = { password: bcrypt.hashSync(password, 10) };
      await model.user.update(userModel, { where: { user_id } });
      successCode(res, data, "Đổi mật khẩu thành công!");
    } else {
      failCode(res, data, "Mật khẩu trùng với mật khẩu cũ!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  logIn,
  register,
  getUserInforById,
  updateUser,
  changePassword,
};
