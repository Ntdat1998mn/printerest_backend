const { successCode, failCode, errorCode } = require("../config/response");
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);

// Lấy thông tin bình luận theo id ảnh
const getCommentById = async (req, res) => {
  try {
    let { image_id } = req.body;
    let data = await model.comment.findAll({ where: { image_id } });
    if (data.length > 0) {
      successCode(res, data, "Lấy bình luận thành công!");
    } else {
      failCode(res, data, "Không có bình luận nào và ảnh này!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};
// Lưu thông tin bình luận của người dùng với hình ảnh
const createComment = async (req, res) => {
  try {
    let { user_id } = req.user.content;
    let { image_id, comment_date, content } = req.body;
    let newComment = {
      image_id,
      user_id,
      comment_date,
      content,
    };
    let data = await model.comment.create(newComment);
    if (data) {
      successCode(res, data, "Tạo bình luận thành công!");
    } else {
      failCode(res, data, "Bình luận thất bại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = { getCommentById, createComment };
