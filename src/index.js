const express = require("express");
const app = express();
app.use(express.json());

const cookieParser = require("cookie-parser");
app.use(cookieParser());

const cors = require("cors");
app.use(cors());
app.listen(8080);
const rootRoute = require("./Routes/rootRoute");
app.use("/api/capstone-1", rootRoute);

// yarn sequelize-auto -d printerest -u root -p 3306 -h localhost -x 1234 -l es6 -o src/models --dialect mysql

const swaggerUi = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

const options = {
  failOnErrors: true, // Whether or not to throw when parsing errors. Defaults to false.
  definition: {
    info: {
      title: "api",
      version: "1.0.0",
    },
  },
  apis: ["src/swagger/index.js"],
};

const openapiSpecification = swaggerJsDoc(options);
app.use("/swagger", swaggerUi.serve, swaggerUi.setup(openapiSpecification));
