const DataTypes = require("sequelize").DataTypes;
const _comment = require("./comment");
const _image = require("./image");
const _image_saving = require("./image_saving");
const _user = require("./user");

function initModels(sequelize) {
  const comment = _comment(sequelize, DataTypes);
  const image = _image(sequelize, DataTypes);
  const image_saving = _image_saving(sequelize, DataTypes);
  const user = _user(sequelize, DataTypes);

  image.belongsToMany(user, { as: 'user_id_users', through: comment, foreignKey: "image_id", otherKey: "user_id" });
  image.belongsToMany(user, { as: 'user_id_user_image_savings', through: image_saving, foreignKey: "image_id", otherKey: "user_id" });
  user.belongsToMany(image, { as: 'image_id_images', through: comment, foreignKey: "user_id", otherKey: "image_id" });
  user.belongsToMany(image, { as: 'image_id_image_image_savings', through: image_saving, foreignKey: "user_id", otherKey: "image_id" });
  comment.belongsTo(image, { as: "image", foreignKey: "image_id"});
  image.hasMany(comment, { as: "comments", foreignKey: "image_id"});
  image_saving.belongsTo(image, { as: "image", foreignKey: "image_id"});
  image.hasMany(image_saving, { as: "image_savings", foreignKey: "image_id"});
  comment.belongsTo(user, { as: "user", foreignKey: "user_id"});
  user.hasMany(comment, { as: "comments", foreignKey: "user_id"});
  image.belongsTo(user, { as: "user", foreignKey: "user_id"});
  user.hasMany(image, { as: "images", foreignKey: "user_id"});
  image_saving.belongsTo(user, { as: "user", foreignKey: "user_id"});
  user.hasMany(image_saving, { as: "image_savings", foreignKey: "user_id"});

  return {
    comment,
    image,
    image_saving,
    user,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
