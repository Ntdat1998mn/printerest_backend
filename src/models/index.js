const Sequelize = require("sequelize");
const {
  dbDatabase,
  dbUser,
  dbPassword,
  dbHost,
  dbPort,
  dbDialect,
} = require("../config/config");
const sequelize = new Sequelize(dbDatabase, dbUser, dbPassword, {
  host: dbHost,
  port: dbPort,
  dialect: dbDialect,
});

module.exports = sequelize;
