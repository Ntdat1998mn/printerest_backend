const jwt = require("jsonwebtoken");
const { privateKey } = require("../config/config");

// Hàm tạo token
const creatToken = (data) => {
  let token = jwt.sign({ content: data }, privateKey, {
    expiresIn: "5m",
    algorithm: "HS256",
  });
  return token;
};

// Hàm check token
const checkToken = (token) => {
  let check = jwt.verify(token, privateKey);
  return check;
};

// Tạo middleware để kiểm tra token, nếu đúng cho truy cập vào dữ liệu

const verifyToken = (req, res, next) => {
  try {
    let token = req.cookies.token;
    let checkedToken = checkToken(token);
    if (checkedToken) {
      req.user = checkedToken;
      next();
    }
  } catch (err) {
    res.status(401).send(err.message);
  }
};

module.exports = {
  creatToken,
  checkToken,
  verifyToken,
};
